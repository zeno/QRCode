package name.zeno.qrcode.library.hui

import android.graphics.Bitmap

/**
 * # 背景图的配置信息
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
class BgImgOptions(
    var bgImg: Bitmap // 背景图
) {

  /** * 背景图样式 */
  var bgImgStyle: Int = OVERRIDE

  /**
   * 背景图宽
   */
  var bgW: Int = 0
    get() {
      return if (field == 0 && bgImgStyle == FILL) {
        bgImg.width
      } else field
    }


  /**
   * 背景图高
   */
  var bgH: Int = 0
    get() {
      return if (field == 0 && bgImgStyle == FILL) {
        bgImg.height
      } else field
    }

  /**
   * if [bgImgStyle] ==  [OVERRIDE]
   * 用于设置二维码的透明度
   */
  var alpha: Float = 0.75F

  /**
   * if [bgImgStyle] ==  [FILL]
   * 用于设置二维码的绘制在背景图上的x坐标
   */
  var startX: Int = 0

  /**
   * if [bgImgStyle] ==  [FILL]
   * 用于设置二维码的绘制在背景图上的y坐标
   */
  var startY: Int = 0

  fun penetrate() = apply {
    this.bgImgStyle
  }
  fun override(alpha: Float) = apply {
    this.bgImgStyle = OVERRIDE
    this.alpha = alpha
  }

  companion object {
    const val FILL = 1      // 将二维码填充在背景图的指定位置
    const val OVERRIDE = 2  // 设置二维码透明度，然后全覆盖背景图
    const val PENETRATE = 3 // 背景图穿透显示, 即二维码主题色为透明，由背景图的颜色进行填充
  }
}

