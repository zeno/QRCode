package name.zeno.qrcode.library.hui.style

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import name.zeno.qrcode.library.hui.ExpandType

/**
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
object TriangleDrawStyle :DrawStyle(){

  // 三角形
  override fun draw(canvas: Canvas, paint: Paint, x: Int, y: Int, w: Int, h: Int, img: Bitmap?) {
    val px = intArrayOf(x, x + (w shr 1), x + w)
    val py = intArrayOf(y + w, y, y + w)
    canvas.drawPolygon(px, py, paint)
  }

  override fun expand(expandType: ExpandType): Boolean {
    return false
  }
}