package name.zeno.qrcode.library.hui.style

import android.graphics.*
import name.zeno.qrcode.library.hui.ExpandType
import name.zeno.qrcode.library.hui.QrCodeOptions

/**
 * # 绘制二维码信息的样式
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
abstract class DrawStyle {
  private val rect = RectF()

  fun rect(l: Int, t: Int, r: Int, b: Int) = rect.apply {
    set(l.toFloat(), t.toFloat(), r.toFloat(), b.toFloat())
  }

  abstract fun draw(canvas: Canvas, paint: Paint, x: Int, y: Int, w: Int, h: Int, img: Bitmap? = null)
  /**
   * 返回是否支持绘制图形的扩展
   *
   * @param expandType
   * @return
   */
  abstract fun expand(expandType: ExpandType): Boolean

  companion object {
    private val path by lazy { Path() }
    fun circle() = CircleDrawStyle
    fun triangle() = TriangleDrawStyle
    fun rect() = RectDrawStyle
    fun diamond() = DiamondDrawStyle
    fun sexangle() = SexangleDrawStyle
    fun octagon() = OctagonDrawStyle
    fun image() = ImageDrawStyle
  }

  fun Canvas.drawPolygon(x: IntArray, y: IntArray, paint: Paint) {
    path.reset()
    path.moveTo(x[0].toFloat(), y[0].toFloat())
    for (i in 0 until x.size) {
      path.lineTo(x[i].toFloat(), y[i].toFloat())
    }
    path.close()
    drawPath(path, paint)
  }

}