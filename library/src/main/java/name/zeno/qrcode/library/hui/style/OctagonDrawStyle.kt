package name.zeno.qrcode.library.hui.style

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import name.zeno.qrcode.library.hui.ExpandType

/**
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
object OctagonDrawStyle : DrawStyle() {
  // 八边形
  override fun draw(canvas: Canvas, paint: Paint, x: Int, y: Int, w: Int, h: Int, img: Bitmap?) {
    val add = w / 3
    val px = intArrayOf(x + add, x + w - add, x + w, x + w, x + w - add, x + add, x, x)
    val py = intArrayOf(y, y, y + add, y + w - add, y + w, y + w, y + w - add, y + add)
    canvas.drawPolygon(px, py, paint)
  }

  override fun expand(expandType: ExpandType): Boolean {
    return expandType == ExpandType.SIZE4
  }
}