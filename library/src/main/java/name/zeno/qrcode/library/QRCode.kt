package name.zeno.qrcode.library

import android.os.Parcel
import android.os.Parcelable

/**
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
class QRCode private constructor(
    val data: String,
    /* 1：相机，2：相册*/
    val from: Int
) : Parcelable {
  constructor(source: Parcel) : this(
      source.readString(),
      source.readInt()
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
    writeString(data)
    writeInt(from)
  }

  companion object {
    const val CAMERA = 1
    const val ALBUM = 2

    @JvmField
    val CREATOR: Parcelable.Creator<QRCode> = object : Parcelable.Creator<QRCode> {
      override fun createFromParcel(source: Parcel): QRCode = QRCode(source)
      override fun newArray(size: Int): Array<QRCode?> = arrayOfNulls(size)
    }

    fun camera(data: String) = QRCode(data, CAMERA)
    fun album(data: String) = QRCode(data, ALBUM)
  }
}