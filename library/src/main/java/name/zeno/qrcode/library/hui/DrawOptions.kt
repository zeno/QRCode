package name.zeno.qrcode.library.hui

import android.graphics.Bitmap
import android.graphics.Color
import name.zeno.qrcode.library.hui.style.DrawStyle
import name.zeno.qrcode.library.hui.style.ImageDrawStyle

/**
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
/** * 绘制二维码的配置信息 */
class DrawOptions {
  /** * 着色颜色 */
  var preColor: Int = Color.BLACK

  /** * 背景颜色 */
  var bgColor: Int = Color.WHITE

  /** * 绘制样式 */
  var drawStyle: DrawStyle = DrawStyle.triangle()


  /**
   * true: 支持对相邻的着色点进行合并处理 （即用一个大图来绘制相邻的两个着色点）
   * 说明： 三角形样式关闭该选项，因为留白过多，对识别有影响
   */
  var enableScale: Boolean = true

  /**
   * 基础图片
   */
  var img: Bitmap? = null

  /**
   * 同一行相邻的两个着色点对应绘制的图片
   */
  var row2Img: Bitmap? = null

  /**
   * 同一列相邻的两个着色点对应绘制的图片
   */
  var col2img: Bitmap? = null

  /**
   * 以(x,y)为左定点的四个着色点对应绘制的图片
   */
  var size4Img: Bitmap? = null


  fun enableScale(expandType: ExpandType): Boolean = when {
    !enableScale -> false
    !drawStyle.expand(expandType) -> false
    drawStyle !is ImageDrawStyle -> false
    expandType == ExpandType.SIZE4 -> size4Img != null
    expandType == ExpandType.COL2 -> col2img != null
    else -> row2Img != null
  }
}

