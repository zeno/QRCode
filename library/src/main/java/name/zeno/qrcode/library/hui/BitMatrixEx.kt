package name.zeno.qrcode.library.hui

import com.google.zxing.qrcode.encoder.ByteMatrix


/**
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
class BitMatrixEx(
    var byteMatrix: ByteMatrix,
    /** 实际生成二维码的宽 */
    var width: Int = 0,
    /** 实际生成二维码的高 */
    var height: Int = 0,
    /** 左白边大小 */
    var leftPadding: Int = 0,
    /** 上白边大小 */
    var topPadding: Int = 0,
    /** 矩阵信息缩放比例 */
    var multiple: Int = 0
)