package name.zeno.qrcode.library.hui.style

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import name.zeno.qrcode.library.hui.ExpandType

/**
 * # 圆点
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
object CircleDrawStyle : DrawStyle() {
  override fun draw(canvas: Canvas, paint: Paint, x: Int, y: Int, w: Int, h: Int, img: Bitmap?) {
    canvas.drawOval(rect(x, y, x + w, y + h), paint)
  }

  override fun expand(expandType: ExpandType): Boolean {
    return expandType == ExpandType.SIZE4
  }
}