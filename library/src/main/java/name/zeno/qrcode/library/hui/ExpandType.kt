package name.zeno.qrcode.library.hui

/**
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
enum class ExpandType {
  // 横向合并
  ROW2,
  // 纵向合并
  COL2,
  // 4 个合并
  SIZE4
}