package name.zeno.qrcode.library

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/12/5
 */
class QRCodeReaderActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_qrcode_reader)

    val fragment = fragmentManager.findFragmentById(R.id.fragmentQrcode) as QRCodeReaderFragment
    fragment.observable().subscribe {
      val data = Intent()
      data.putExtra("data", it)
      setResult(RESULT_OK, data)
      finish()
    }
  }
}