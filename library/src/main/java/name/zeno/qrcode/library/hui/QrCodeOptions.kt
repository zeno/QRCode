package name.zeno.qrcode.library.hui

import android.graphics.Bitmap
import android.graphics.Color
import android.support.annotation.ColorInt
import com.google.zxing.EncodeHintType
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel


/**
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
class QrCodeOptions(
    var msg: String,            //塞入二维码的信息
    var w: Int = 400,           //生成二维码的宽
    var h: Int = w,             //生成二维码的高

    /** 二维码信息(即传统二维码中的黑色方块) 绘制选项 */
    var drawOptions: DrawOptions = DrawOptions()
) {
  /** 背景图样式选项 */
  var bgImgOptions: BgImgOptions? = null


  //<editor-fold desc="logo 定制属性">
  var logo: Bitmap? = null                        // logo 图片
  @ColorInt
  var logoBgColor: Int = Color.TRANSPARENT        // logo 底色
  @ColorInt
  var logoBorderColor: Int = Color.TRANSPARENT    // 边框颜色
  var logoRate: Float = 0F                        // logo 占二维码的比例
  var logoRound: Int = 0                          // logo 圆角半径
  //</editor-fold>

  //<editor-fold desc="探测点">
  var detectImg: Bitmap? = null               // 探测图形，优先级高于颜色的定制（即存在图片时，用图片绘制探测图形）
  var detectRound: Int = 100                    // 探测点圆角
  @ColorInt
  var detectOutColor: Int = Color.BLACK       // 外圈颜色
  @ColorInt
  var detectInColor: Int = Color.BLACK        // 内圈颜色
  //</editor-fold>

  var hints = HashMap<EncodeHintType, Any>()

  init {
    hints[EncodeHintType.CHARACTER_SET] = "utf-8"
    hints[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.M
  }

  /**
   * @param logo
   * @param rate logo 占二维码的比例
   * @param round logo 圆角大小
   */
  fun logo(logo: Bitmap, rate: Float = 0.2F, round: Int = 0,
           @ColorInt bgColor: Int = Color.TRANSPARENT,
           @ColorInt borderColor: Int = Color.TRANSPARENT
  ) = apply {
    this.logo = logo
    this.logoRate = rate
    this.logoRound = round
    this.logoBorderColor = borderColor
    this.logoBgColor = bgColor
  }

  /**
   * # 指定图片作为探测点
   *
   * ```
   * ##   ##
   * ##   ##
   *
   * ##
   * ##
   * ```
   */
  fun detect(bmp: Bitmap) = apply {
    this.detectImg = bmp
  }

  /**
   * # 指定探测点颜色
   * ```
   * ##   ##
   * ##   ##
   *
   * ##
   * ##
   * ```
   */
  fun detect(@ColorInt innerColor: Int, @ColorInt outerColor: Int = innerColor) = apply {
    this.detectInColor = innerColor
    this.detectOutColor = outerColor
  }
}