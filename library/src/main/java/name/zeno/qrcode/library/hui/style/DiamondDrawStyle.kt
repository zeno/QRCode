package name.zeno.qrcode.library.hui.style

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import name.zeno.qrcode.library.hui.ExpandType

/**
 * # 五边形-钻石
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
object DiamondDrawStyle : DrawStyle() {
  override fun draw(canvas: Canvas, paint: Paint, x: Int, y: Int, w: Int, h: Int, img: Bitmap?) {
    val cell4 = w shr 2
    val cell2 = w shr 1
    val px = intArrayOf(x + cell4, x + w - cell4, x + w, x + cell2, x)
    val py = intArrayOf(y, y, y + cell2, y + w, y + cell2)
    canvas.drawPolygon(px, py, paint)
  }

  override fun expand(expandType: ExpandType): Boolean {
    return expandType == ExpandType.SIZE4
  }
}