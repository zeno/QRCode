package name.zeno.qrcode.library.hui.style

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import name.zeno.qrcode.library.hui.ExpandType

/**
 * @author 陈治谋 (513500085@qq.com)
 * @since 2017/11/30
 */
object ImageDrawStyle : DrawStyle() {
  // 自定义图片
  override fun draw(canvas: Canvas, paint: Paint, x: Int, y: Int, w: Int, h: Int, img: Bitmap?) {
    canvas.drawBitmap(img, null, Rect(x, y, x + w, y + h), paint)
  }

  override fun expand(expandType: ExpandType): Boolean {
    return true
  }
}